package example;

import org.kie.api.runtime.process.ProcessContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DebugSnippet {
	
    private static Logger LOGGER = LoggerFactory.getLogger(DebugSnippet.class);
    
	public static void snippet(ProcessContext kcontext) {
		LOGGER.info("piid: "+kcontext.getProcessInstance().getId());
	}
}
